fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

## Choose your installation method:

<table width="100%" >
<tr>
<th width="33%"><a href="http://brew.sh">Homebrew</a></td>
<th width="33%">Installer Script</td>
<th width="33%">Rubygems</td>
</tr>
<tr>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS or Linux with Ruby 2.0.0 or above</td>
</tr>
<tr>
<td width="33%"><code>brew cask install fastlane</code></td>
<td width="33%"><a href="https://download.fastlane.tools">Download the zip file</a>. Then double click on the <code>install</code> script (or run it in a terminal window).</td>
<td width="33%"><code>sudo gem install fastlane -NV</code></td>
</tr>
</table>

# Available Actions
## iOS
### ios ci_dev
```
fastlane ios ci_dev
```
Cleans, tests, builds, computes code coverage, lints and analyses complexity
### ios ci_qa
```
fastlane ios ci_qa
```
Cleans, tests, builds, archive and upload builds
### ios ci_drop
```
fastlane ios ci_drop
```
Cleans, tests, builds, archive and upload builds
### ios build_archive_appstore
```
fastlane ios build_archive_appstore
```
Build and archive an AppStore build
### ios build_archive_qa
```
fastlane ios build_archive_qa
```
Build and archive a QA build
### ios build_archive_uat
```
fastlane ios build_archive_uat
```
Build and archive a UAT build
### ios clean
```
fastlane ios clean
```
Step to clean data generated by CI build steps
### ios clean_all
```
fastlane ios clean_all
```
Clean all data generated by CI build steps
### ios clean_xcode
```
fastlane ios clean_xcode
```
Clean data generated by Xcode compilation
### ios install_profiles
```
fastlane ios install_profiles
```
Step to install the profiles
### ios open_profiles
```
fastlane ios open_profiles
```
Open provisioning profiles as to copy them to `~/Library/MobileDevice/Provisioning Profiles/`
### ios recodesign_drop
```
fastlane ios recodesign_drop
```
Recodesign the appstore build into a testable one
### ios complexity_analysis
```
fastlane ios complexity_analysis
```
Run complexity analysis tool on the Source directory
### ios coverage
```
fastlane ios coverage
```
Run code coverage analysis tool on the data generated by tests
### ios lint
```
fastlane ios lint
```
Run lint analysis tool
### ios set_version_qa
```
fastlane ios set_version_qa
```
Set version number for QA builds
### ios set_version_drop
```
fastlane ios set_version_drop
```
Set version number for Release candidate builds
### ios set_version_drop_uat
```
fastlane ios set_version_drop_uat
```
Set version number for UAT builds
### ios test
```
fastlane ios test
```
Step to run tests
### ios unit_test
```
fastlane ios unit_test
```
Run unit tests
### ios upload_drop
```
fastlane ios upload_drop
```
Upload testable release candidate build to distribution server
### ios upload_drop_uat
```
fastlane ios upload_drop_uat
```
Upload UAT build to distribution server
### ios upload_qa
```
fastlane ios upload_qa
```
Upload qa build to distribution server
### ios validate_qa
```
fastlane ios validate_qa
```

### ios validate_drop_uat
```
fastlane ios validate_drop_uat
```

### ios validate_drop
```
fastlane ios validate_drop
```

### ios validate_appstore
```
fastlane ios validate_appstore
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
