# Starting a new Project

## Prerequisites

* Admin privileges for:
    * Bitbucket
    * HockeyApp
    * Jenkins
* An Apple Developer account already setup with the required code signing identities and provisioning profiles

If you do not have access to the above, please pair with a team lead to complete this guide.


## Bitbucket Repository Setup

1. Create a new repo:
    * Select **outware** as the *Owner*
    * Select **Outware's Project** as the *Project*
    * Follow the naming convention: *<CodeName - iOS>*
    * Make sure the repository is **private**
    * Set a `Swift` language
1. Create a new Bitbucket Team (e.g.: *Team-CodeName-iOS*).
    * Make sure the group does not get access to any other repositories
1. Add the new team to the repo with *Write* access


## HockeyApp Setup

The following steps will only depict a high level guide to creating new apps on HockeyApp.

1. Create 2 new apps: *ProjectName - OMQA* and *ProjectName - DropClient*
1. Add the developers, project manager and client to the apps
1. Create the API tokens
1. Update the API tokens under `fastlane/.env`


## Jenkins Setup

By default, each project should have 3 jobs:

* ProjectName-iOS-Dev
* ProjectName-iOS-OMQA
* ProjectName-iOS-DropClient

Create a new Jenkins job for each of the above by copying the following, respectively:

* OMStarWars-iOS-Dev
* OMStarWars-iOS-OMQA
* OMStarWars-iOS-DropClient

Update the git repository, git branch being built and the `project_name` configurations for each of them.

## Repository Setup

### OMStarWars Forking

1. Clone OMStarWars

        git clone git@bitbucket.org:outware/omproject-ios-swift.git <project name>

1. Set the new remote

        git remote set-url origin <new repo url>

1. Push to the new remote

        git push --set-upstream

1. Create a `master` branch on the new remote

        git checkout -b master

        git push --set-upstream

### OMStarWars Renaming

Choose from one of the following approaches.

#### Automated Steps

1. Delete the "DerivedData" folder
1. Run the renaming script

      ./renameProject.sh OMStarWars <ProjectName>

#### Manual Steps

1. Checkout on a new branch

        git checkout -b feature/project-rename

1. Select the project in the *Project Navigator* and press *Enter* to rename it
1. Rename the scheme
1. Change the `PROJECT_NAME` setting under **Project** (not Target) > **Build Settings**
1. Update the `import` statements referencing *OMStarWars* (e.g.: `@testable import OMStarWars` -> `@testable import <ProjectName>`)
1. Close Xcode
1. Rename `OMStarWars.xcworkspace` file to `<ProjectName>.xcworkspace`
1. Update the location of the project in the `<ProjectName>.xcworkspace/contents.xcworkspacedata` file to use a relative path: `location = "group:<ProjectName>.xcodeproj"`
1. Open the workspace and run the tests
1. Review the changes, commit them and open a Pull Request

## Fastlane Setup

1. Checkout on a new branch

        git checkout -b feature/fastlane-setup

1. Install required ruby dependencies

        bundle install --local --path vendor/bundle

1. Edit the `fastlane/.env` file to customise *Fastlane* to your project.

## Last but not least

* Delete the renaming script `renameProject.sh`
* Customise [README.md](../README.md) to your project
* Delete this guide
